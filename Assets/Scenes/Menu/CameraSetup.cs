﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraSetup : MonoBehaviour
{


    public Camera Camera;

    public GameObject UIText;

    void Start()
    {
        StartCoroutine(nameof(TextComing));
        
    }


    public IEnumerator TextComing()
    {
        var rt = UIText.GetComponent<RectTransform>();
        var target = Vector2.zero;
        var velocity = Vector2.zero;
        do
        {
            yield return null;
            rt.localPosition = Vector2.Lerp(rt.localPosition, target , 0.04f);
            Debug.Log(velocity.magnitude);
        } while (Math.Round(rt.localPosition.magnitude, 1) != target.magnitude);
        rt.localPosition = target;
        Debug.Log("Animation ended");

    }


    private void Update()
    {
        var scene = SceneManager.GetActiveScene();
#if UNITY_STANDALONE
        if (Input.GetKey(KeyCode.Space))
            SceneManager.LoadSceneAsync(scene.buildIndex + 1);
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0)
            SceneManager.LoadSceneAsync(scene.buildIndex + 1);


#endif
    }


}
