﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChecstScript : MonoBehaviour
{
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            animator.SetTrigger("Open");
            var bound = this.gameObject.GetObjectBounds();
            var tipCanvas = GameObject.Find("TipCanvas");
            var rectTransform = tipCanvas.GetComponent<RectTransform>();
            var canvasGroup = tipCanvas.GetComponent<CanvasGroup>();
            tipCanvas.transform.position = this.transform.position + bound / 2f + new Vector3(0.1f, 0.1f, 0);
            tipCanvas.transform.Find("Header").GetComponent<Text>().text = "Red Chest";
            tipCanvas.transform.Find("Description").GetComponent<Text>().text = "Created by Ann";
            canvasGroup.alpha = 1;
            StopCoroutine(nameof(fade));
            StartCoroutine(nameof(fade));
        }
    }

    IEnumerator fade()
    {
        var tipCanvas = GameObject.Find("TipCanvas");
        var canvasGroup = tipCanvas.GetComponent<CanvasGroup>();
        yield return new WaitForSeconds(2f);
        for (int i = 1; i <= 10; i++)
        {
            canvasGroup.alpha = 1f - i / 10f;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
