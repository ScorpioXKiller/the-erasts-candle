﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public float speed = 1f;

    public GameObject toFollow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        var t = transform.position;
        //var xdiff = t.x - toFollow.transform.position.x;
        //var ydiff = t.y - toFollow.transform.position.y;

        //if (xdiff > speed)
        //    t.x -= speed;
        //else if (xdiff < -speed)
        //    t.x += speed;
        //if (ydiff > speed)
        //    t.y -= speed;
        //else if (ydiff < -speed)
        //    t.y += speed;
        t.x = toFollow.transform.position.x;
        t.y = toFollow.transform.position.y;


        transform.position = t;


    }
}
