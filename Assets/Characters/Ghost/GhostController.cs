﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GhostController : MonoBehaviour
{
    [SerializeField] private LayerMask dashLayerMask;
    public GameObject ghost;
    //public GameObject DASH;
    public float moveSpeed = 0.5f;
    private bool dash;

    public Rigidbody2D rb;
    public Animator animator;
    Vector2 move;

    //[SerializeField] private Transform DashEffect;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        move.x = Input.GetAxisRaw("Horizontal");
        move.y = Input.GetAxisRaw("Vertical");


        if (Input.GetKey(KeyCode.LeftShift) || (Input.GetKey(KeyCode.RightShift)))
        {
            moveSpeed = 300f;
        }

        else
        {
            moveSpeed = 150f;
        }

        if (move.x == -1)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                dash = true;
                animator.SetBool("Push", true);
                animator.SetFloat("Left", 1);
                StartCoroutine(inst_Obj());

                //Instantiate(DASH, DASH.transform.position, Quaternion.identity);
            }
            else
            {
                animator.SetBool("Push", false);
                animator.SetFloat("Left", 0);
            }
        }

        IEnumerator inst_Obj()
        {
            yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        }


        /*if(move.x == -1)
        {
            if (Input.GetKey(KeyCode.P))
            {
                animator.SetBool("Push", true);
                animator.SetFloat("Left", 1);
                
            }
            else
            {
                animator.SetBool("Push", false);
                animator.SetFloat("Left", 0);
            }

        }*/





        animator.SetFloat("Horizontal", move.x);
        animator.SetFloat("Vertical", move.y);
        animator.SetFloat("Speed", move.sqrMagnitude);


    }

    void FixedUpdate()
    {
        //rb.MovePosition(rb.position + move * moveSpeed * Time.fixedDeltaTime);
        rb.velocity = move * moveSpeed;

        if (dash)
        {
            float dashAmount = 10f;
            Vector3 dashPosition = rb.position + move * dashAmount;

            RaycastHit2D raycastHit2D = Physics2D.Raycast(transform.position, move, dashAmount, dashLayerMask);
            if (raycastHit2D.collider != null)
            {
                dashPosition = raycastHit2D.point;
            }

            rb.MovePosition(dashPosition);
            //rb.MovePosition(rb.position + move * dashAmount);
            dash = false;
        }
    }

}
