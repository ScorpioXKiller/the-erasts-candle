﻿using Assets.Characters;
using System.Collections;
using System.Linq;
using UnityEngine;

public class GhostScript : Character
{
    public int ItemLayerForSave;

    //public float speed = 200f;
    private Animator animator;
    SpriteRenderer spriteRenderer;

    public float dashSpeed = 10f;
    bool IsDashing = false;
    // Use this for initialization

    void Start()
    {
        animator = GetComponent<Animator>();
        rbody = GetComponent<Rigidbody2D>();
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    protected void Update()
    {
        if (!IsDashing)
        {
            SetDirection();
            animator.SetFloat("x", direction.x);
            animator.SetFloat("y", direction.y);
            Move(rbody);


            if (Input.GetKeyDown(KeyCode.Space))
            {
                animator.SetBool("Dash", true);
                IsDashing = true;
                StartCoroutine(Dash());
            
            }

            else
            {
                animator.SetBool("Dash", false);
            }
        }
    }

    IEnumerator Dash()
    {
        rbody.velocity = direction * dashSpeed;
        animator.Play("Dash");
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        IsDashing = false;
    }

    void SetDirection()
    {
        var t = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        var magn = t.magnitude;
        animator.SetFloat("Magnitude", magn);
        if (magn > 1f)
            direction = t.normalized;
        else
            direction = t;
    }

    //private void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (other.gameObject.layer == LayerMask.NameToLayer("Enterier"))
    //    {
    //        ItemLayerForSave = other.gameObject.GetComponent<SpriteRenderer>().sortingOrder;
    //        other.gameObject.GetComponent<SpriteRenderer>().sortingOrder = sort + 1;
    //    }
    //}
    //private void OnTriggerExit2D(Collider2D other)
    //{
    //    if (other.gameObject.layer == LayerMask.NameToLayer("Enterier"))
    //    {
    //        other.gameObject.GetComponent<SpriteRenderer>().sortingOrder = ItemLayerForSave;
    //    }
    //}
}