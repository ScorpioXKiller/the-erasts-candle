﻿
namespace Assets.Characters.Enemies
{
    using Assets.Scripts.PathFinding;
    using System;
    using System.Collections;
    using UnityEngine;
    using Grid = Scripts.PathFinding.Grid;

    class Enemy : Character
    {
        [SerializeField]
        private GameObject target;


        const float minPathUpdateTime = .2f;
        const float pathUpdateMoveThreshold = .5f;
        const float cameThreshold = .2f;
        readonly float sqrMoveThreshold = pathUpdateMoveThreshold * pathUpdateMoveThreshold;

        Vector2[] path;

        private void Start()
        {
            rbody = GetComponent<Rigidbody2D>();
            StartFollow();
        }

        void StartFollow()
        {
            var t = FindObjectOfType<MazeGenerateScript>();
            StartCoroutine(nameof(UpdatePath));
        }

        public void OnPathFound(Vector2[] path, bool pathSuccessfull)
        {
            if (pathSuccessfull)
            {
                this.path = path;
                StopCoroutine(nameof(FollowPath));
                StartCoroutine(nameof(FollowPath));
            }
        }

        IEnumerator UpdatePath()
        {
            if (!Grid.isInited)
                yield return new WaitUntil(() => Grid.isInited);
            PathRequestManager.RequestPath(new PathRequest(transform.position, target.transform.position, OnPathFound));
            Vector3 targetPosOld = target.transform.position;// + new Vector3(sqrMoveThreshold, sqrMoveThreshold, sqrMoveThreshold);

            do
            {

                yield return new WaitForSeconds(minPathUpdateTime);
                if ((target.transform.position - targetPosOld).sqrMagnitude > sqrMoveThreshold)
                {

                    PathRequestManager.RequestPath(new PathRequest(transform.position, target.transform.position, OnPathFound));
                    targetPosOld = target.transform.position;
                }
            } while (true);

        }

        IEnumerator FollowPath()
        {

            //bool followingPath = true;
            int pathIndex = 0;

            do
            {
                MoveTo(rbody, path[pathIndex]);
                if (Math.Abs(path[pathIndex].y - this.transform.position.y) < cameThreshold
                    && Math.Abs(path[pathIndex].x - this.transform.position.x) < cameThreshold)
                    ++pathIndex;
                yield return null;
            } while (path.Length > pathIndex);

        }






        //IEnumerator FollowCoroutine()
        //{

        //    yield return new WaitUntil(() => pathFinding.grid.isInited);
        //    UnityEngine.Debug.Log($"{timer.ElapsedMilliseconds} time to build grid");
        //    List<Node> nodes = new List<Node>();
        //    do
        //    {

        //        Node startNode = pathFinding.grid.GetNodeFromWorldPoint(this.transform.position);
        //        Node targetNode = pathFinding.grid.GetNodeFromWorldPoint(this.target.transform.position);
        //        var x = this.transform.position.x - this.target.transform.position.x;
        //        var y = this.transform.position.y - this.target.transform.position.y;
        //        timer.Restart();
        //        float dist = Math.Abs(x) + Math.Abs(y);

        //        if (pathFinding.FindPath(startNode, targetNode, out nodes))
        //        {
        //           // UnityEngine.Debug.Log(dist);                   
        //            MoveTo(rbody, nodes[0].position);
        //            UnityEngine.Debug.Log($"{timer.ElapsedMilliseconds} pathfinder cycle.");
        //            // yield return new WaitUntil(()=> !IsMoving);
        //            yield return null;
        //        }
        //        else if(dist > 1.0f)
        //        {
        //            MoveTo(rbody, this.target.transform.position);
        //            UnityEngine.Debug.Log($"{timer.ElapsedMilliseconds} false path or nothing.");
        //            yield return null;
        //        }
        //        else
        //        {
        //            MoveTo(rbody, rbody.transform.position);
        //            yield return null;
        //        }

        //    } while (true);
        //}
    }
}
