﻿namespace Assets.Characters
{

    using UnityEngine;

    public abstract class Character : MonoBehaviour
    {
        [SerializeField]
        private float Speed = 10f;

        protected bool IsMoving = false;

        protected Vector2 direction;

        protected Rigidbody2D rbody;

        public void MoveTo(Vector2 vector)
        {
            IsMoving = true;
            transform.position = Vector2.MoveTowards(transform.position, vector, Speed * Time.deltaTime);
            IsMoving = false;
        }

        public void MoveTo(Rigidbody2D rbody, Vector2 vector)
        {
            IsMoving = true;
            var pos = Vector2.MoveTowards(rbody.position, vector, Speed * Time.deltaTime);
            rbody.MovePosition(pos);
            IsMoving = false;
        }

        public void Move(Rigidbody2D rbody)
        {
            IsMoving = true;
            rbody.MovePosition(rbody.position + direction * Speed * Time.deltaTime);
            IsMoving = false;
        }
    }
}