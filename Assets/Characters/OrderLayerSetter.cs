﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Characters 
{
    public class OrderLayerSetter : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;
        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        } 
        public void LateUpdate()
        {
            spriteRenderer.sortingOrder = (int)(this.gameObject.transform.position.y * -100f);
        }
    }
}
