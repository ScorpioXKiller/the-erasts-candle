﻿namespace Assets.Scripts.PathFinding
{
    using Assets.Scripts.BinaryTreeModel;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using UnityEngine;


    public class PathFinding : MonoBehaviour
    {
        [SerializeField]
        Grid grid;

        //Stopwatch timer = new Stopwatch();


        public void FindPath(PathRequest request, Action<PathResult> callback)
        {

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            var step = Grid.mazeGrid[3, 3].position - Grid.mazeGrid[1, 1].position;
            int sI = (int)(request.pathStart.x / step.x);
            int sJ = (int)(request.pathStart.y / step.y);
            int tI = (int)(request.pathEnd.x / step.x);
            int tJ = (int)(request.pathEnd.y / step.y);


            Node startNode = Grid.mazeGrid[sJ * 2 + 1, sI * 2 + 1];
            Node targetNode = Grid.mazeGrid[tJ * 2 + 1, tI * 2 + 1];

            UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds + "ms to prepare");
            stopwatch.Restart();
            bool bigRes = false;
            bool smallRes = false;
            List<Node> waypoint;
            lock (Grid.mazeGrid)
                bigRes = FindPath(startNode, targetNode, Grid.mazeGrid, out waypoint);

            UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds + "ms to solve maze;");
            stopwatch.Restart();
            waypoint = waypoint.Where(w => w.gridX % 2 == 1 && w.gridY % 2 == 1).ToList();



            UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds + "ms to centre rooms;");


            stopwatch.Restart();
            List<Vector2> path = new List<Vector2>();

            List<Node> tempPath2;
            lock (Grid.grid)
            {
                startNode = grid.GetNodeFromWorldPoint(request.pathStart);
                if (startNode.isWall)
                    startNode = grid.GetNeighbours(startNode, Grid.grid).FirstOrDefault(f => !f.isWall);
                var count = waypoint.Count;


                if (count > 1)
                    for (int i = 0; i < waypoint.Count; i++)
                    {
                        targetNode = grid.GetNodeFromWorldPoint(waypoint[i].position);
                        FindPath(startNode, targetNode, Grid.grid, out List<Node> tempPath);
                        path.AddRange(tempPath.Select(s => s.position).ToList());
                        startNode = targetNode;
                    }
                targetNode = grid.GetNodeFromWorldPoint(request.pathEnd);
                if (targetNode.isWall)
                    targetNode = grid.GetNeighbours(targetNode, Grid.grid).FirstOrDefault(f => !f.isWall);
                smallRes = FindPath(startNode, targetNode, Grid.grid, out tempPath2);
            }
            UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds + "ms to find path;");
            stopwatch.Restart();

            path.AddRange(tempPath2.Select(s => s.position).ToList());
            UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds + "to rework path");
            stopwatch.Stop();

            callback(new PathResult(path.ToArray(), bigRes | smallRes, request.callback));




        }
        public bool FindPath(Node startNode, Node targetNode, Node[,] map, out List<Node> waypoints)
        {
            // timer.Restart();
            waypoints = new List<Node>();

            //Node startNode = grid.GetNodeFromWorldPoint(request.pathStart);
            //Node targetNode = grid.GetNodeFromWorldPoint(request.pathEnd); 
            if (startNode is null || targetNode is null || map is null)
                return false;

            startNode.parent = startNode;

            if (startNode.isWall || targetNode.isWall)
            {
                return false;
            }

            Heap<Node> openSet = new Heap<Node>(map.Length);
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {

                Node node = openSet.RemoveFirst();
                closedSet.Add(node);

                if (node == targetNode)
                {
                    waypoints = RetracePath(startNode, targetNode);
                    // UnityEngine.Debug.Log($"{timer.ElapsedMilliseconds}ms to find path");
                    return waypoints.Count > 0;
                }


                foreach (Node neighbour in grid.GetNeighbours(node, map))
                {
                    if (neighbour.isWall || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                    if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newCostToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = node;

                        if (!openSet.Contains(neighbour))
                            openSet.Add(neighbour);
                        else
                            openSet.UpdateItem(neighbour);
                    }
                }
            }

            return false;
            // callback(new PathResult(waypoints.ToArray(), pathSuccess));
        }

        List<Node> RetracePath(Node start, Node target)
        {
            List<Node> path = new List<Node>();
            Node curNode = target;
            while (curNode != start)
            {
                path.Add(curNode);
                curNode = curNode.parent;
            }
            path.Reverse();
            //grid.path = path;
            return path;
        }

        public int GetDistance(Node nodeA, Node nodeB)
        {
            int dstX = Math.Abs(nodeA.gridX - nodeB.gridX);
            int dstY = Math.Abs(nodeA.gridY - nodeB.gridY);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            return 14 * dstX + 10 * (dstY - dstX);
        }
    }
}
