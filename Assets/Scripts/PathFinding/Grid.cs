﻿namespace Assets.Scripts.PathFinding
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;


    public class Grid : MonoBehaviour
    {
        [NonSerialized]
        public static bool isInited = false;

        [NonSerialized]
        public volatile static Node[,] grid;
        //[NonSerialized]
        //public int MaxSize;
        public volatile static Node[,] mazeGrid;

        public List<LayerMask> unWalkableMasks;

        public float nodeSize;

        Vector2 gridWorldSize;

        //public List<Node> path;
        //Vector2 gridWorldSize;

        public void SetGridSizeAndCreate(Vector2 value, Node[,] maze)
        {
            gridWorldSize.x = (int)Math.Ceiling(value.x / nodeSize) * nodeSize;
            gridWorldSize.y = (int)Math.Ceiling(value.y / nodeSize) * nodeSize;
            grid = new Node[(int)Math.Ceiling(value.x / nodeSize), (int)Math.Ceiling(value.y / nodeSize)];
            //MaxSize = grid.GetLength(0) * grid.GetLength(1);
            var nodeRadius = nodeSize / 2.0f;
            var radiusForGrid = nodeRadius; /** 0.8f;*/
            var sizeForGrid = nodeSize * 0.8f;
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    Vector2 worldPoint = new Vector2(i * nodeSize + nodeRadius, -(j * nodeSize + nodeRadius));
                    var UnWalkable = unWalkableMasks.Any(layer => Physics2D.OverlapBox(
                      worldPoint,
                      new Vector2(sizeForGrid, sizeForGrid),
                      0, 
                      layer));
                    grid[i, j] = new Node(worldPoint, UnWalkable, i, j);
                }
            }
            mazeGrid = maze;

            isInited = true;
        }

        public Node GetNodeFromWorldPoint(Vector2 worldPoint)
        {

            int x = (int)Math.Floor(worldPoint.x / nodeSize);
            int y = (int)Math.Floor(-worldPoint.y / nodeSize);
            var node = grid[x, y];
            if (node.isWall)
            {
                var temp = GetNeighbours(node, grid).FirstOrDefault(w => !w.isWall);
                if (temp != null)
                    node = temp;
            }
            return node;

        }

        public List<Node> GetNeighbours(Node node, Node[,] nodes)
        {            
            List<Node> neighbours = new List<Node>();
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0) continue;
                    int checkX = node.gridX + i;
                    int checkY = node.gridY + j;

                    if (checkX >= 0 && checkX < nodes.GetLength(0) && checkY >= 0 && checkY < nodes.GetLength(1))
                    {
                        neighbours.Add(nodes[checkX, checkY]);
                    }
                }
            }
            return neighbours;
        }

        //private void OnDrawGizmos()
        //{
        //    Gizmos.DrawWireCube(new Vector2(gridWorldSize.x / 2, -gridWorldSize.y / 2), new Vector2(gridWorldSize.x, -gridWorldSize.y));
        //    if (grid != null)
        //    {

        //        foreach (var item in grid)
        //        {

        //            Gizmos.color = item.isWall ? Color.red : Color.white;
                   
        //            Gizmos.DrawCube(item.position, Vector3.one * nodeSize);
        //        }

        //    }
        //}
    }
}
