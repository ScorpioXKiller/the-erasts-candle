﻿namespace Assets.Scripts.PathFinding
{
    using Assets.Scripts.BinaryTreeModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using UnityEngine;

    public class Node : IHeapItem<Node>
    {
        public int gridX;
        public int gridY;
        public int gCost;
        public int hCost;
        public int fCost => gCost + hCost;


        int heapIndex;

        public Node parent;

        public Vector2 position;
        public bool isWall;
        public Node(Vector2 position, bool isWall, int gridX, int gridY)
        {
            this.position = position;
            this.isWall = isWall;
            this.gridX = gridX;
            this.gridY = gridY;
        }

        public int HeapIndex
        {
            get
            {
                return heapIndex;
            }
            set
            {
                heapIndex = value;
            }
        }

        public int CompareTo(Node nodeToCompare)
        {
            int compare = fCost.CompareTo(nodeToCompare.fCost);
            if (compare == 0)
            {
                compare = hCost.CompareTo(nodeToCompare.hCost);
            }
            return -compare;
        }
    }
}

