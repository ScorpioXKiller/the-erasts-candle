﻿namespace Assets.Scripts.PathFinding
{
    public struct PathNode
    {
        public int gCost;
        public int hCost;
        public int fCost => gCost + hCost;
        public int gridX;
        public int gridY;
        public PathNode(int gridX, int gridY)
        {
            this.gCost = 0;
            this.hCost = 0;
            this.gridX = gridX;
            this.gridY = gridY;
        }
    }
}

