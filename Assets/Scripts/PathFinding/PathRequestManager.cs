﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.PathFinding
{
    public class PathRequestManager : MonoBehaviour
    {

        Queue<PathResult> results = new Queue<PathResult>();

        static PathRequestManager instance;
        [SerializeField]
        PathFinding pathfinding;

        void Awake()
        {
            instance = this;
            pathfinding = GetComponent<PathFinding>();
        }

        void Update()
        {
            if (results.Count > 0)
            {
                int itemsInQueue = results.Count;
                lock (results)
                {
                    for (int i = 0; i < itemsInQueue; i++)
                    {
                        PathResult result = results.Dequeue();
                        result.callback(result.path, result.success);
                    }
                }
            }
        }
        public static async void RequestPath(PathRequest request)
        {
            await Task.Run(() => instance.pathfinding.FindPath(request, instance.FinishedProcessingPath));
            //ThreadStart threadStart = delegate
            //{
            //    instance.pathfinding.FindPath(request, instance.FinishedProcessingPath);
            //};
            //threadStart.Invoke();
        }

        public void FinishedProcessingPath(PathResult result)
        {
            lock (results)
            {
                results.Enqueue(result);
            }
        }



    }

    public struct PathResult
    {
        public Vector2[] path;
        public bool success;
        public Action<Vector2[], bool> callback;

        public PathResult(Vector2[] path, bool success, Action<Vector2[], bool> callback)
        {
            this.path = path;
            this.success = success;
            this.callback = callback;
        }

    }

    public struct PathRequest
    {
        public Vector2 pathStart;
        public Vector2 pathEnd;
        public Action<Vector2[], bool> callback;

        public PathRequest(Vector2 pathStart, Vector2 pathEnd, Action<Vector2[], bool> callback)
        {
            this.pathStart = pathStart;
            this.pathEnd = pathEnd;
            this.callback = callback;
        }

    }
}
