﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.BinaryTreeModel
{
    public enum BinSide
    {
        Left,
        Right
    }
    public class BinaryTree<T> where T : IComparable
    {
        public T Data { get; private set; }
        public BinaryTree<T> Left { get; set; }
        public BinaryTree<T> Right { get; set; }
        public BinaryTree<T> Parent { get; set; }

        public void Insert(T data)
        {
            if (Data == null || Data.Equals(data))
            {
                Data = data;
                return;
            }
            if (Data.CompareTo(data) == 1)
            {
                if (Left == null) Left = new BinaryTree<T>();
                Insert(data, Left, this);
            }
            else
            {
                if (Right == null) Right = new BinaryTree<T>();
                Insert(data, Right, this);
            }
        }

        /// <summary>
        /// Вставляет значение в определённый узел дерева
        /// </summary>
        /// <param name="data">Значение</param>
        /// <param name="node">Целевой узел для вставки</param>
        /// <param name="parent">Родительский узел</param>
        private void Insert(T data, BinaryTree<T> node, BinaryTree<T> parent)
        {

            if (node.Data == null || node.Data.Equals(data))
            {
                node.Data = data;
                node.Parent = parent;
                return;
            }
            if (node.Data.CompareTo(data) == 1)
            {
                if (node.Left == null) node.Left = new BinaryTree<T>();
                Insert(data, node.Left, node);
            }
            else
            {
                if (node.Right == null) node.Right = new BinaryTree<T>();
                Insert(data, node.Right, node);
            }
        }
        /// <summary>
        /// Уставляет узел в определённый узел дерева
        /// </summary>
        /// <param name="data">Вставляемый узел</param>
        /// <param name="node">Целевой узел</param>
        /// <param name="parent">Родительский узел</param>
        private void Insert(BinaryTree<T> data, BinaryTree<T> node, BinaryTree<T> parent)
        {

            if (node.Data == null || node.Data.Equals(data))
            {
                node.Data = data.Data;
                node.Left = data.Left;
                node.Right = data.Right;
                node.Parent = parent;
                return;
            }
            if (node.Data.CompareTo(data) == 1)
            {
                if (node.Left == null) node.Left = new BinaryTree<T>();
                Insert(data, node.Left, node);
            }
            else
            {
                if (node.Right == null) node.Right = new BinaryTree<T>();
                Insert(data, node.Right, node);
            }
        }
        public BinaryTree<T> Find(T data)
        {
            if (Data.Equals(data)) return this;
            if (Data.CompareTo(data) == 1)
            {
                return Find(data, Left);
            }
            return Find(data, Right);
        }
        public BinaryTree<T> Find(T data, BinaryTree<T> node)
        {
            if (node == null) return null;

            if (node.Data.Equals(data)) return node;
            if (node.Data.CompareTo(data) == 1)
            {
                return Find(data, node.Left);
            }
            return Find(data, node.Right);
        }
        private BinSide? MeForParent(BinaryTree<T> node)
        {
            if (node.Parent == null) return null;
            if (node.Parent.Left.Equals(node)) return BinSide.Left;
            if (node.Parent.Right.Equals(node)) return BinSide.Right;
            return null;
        }
        public void Remove(BinaryTree<T> node)
        {
            if (node == null) return;
            var me = MeForParent(node);
            //Если у узла нет дочерних элементов, его можно смело удалять
            if (node.Left == null && node.Right == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = null;
                }
                else
                {
                    node.Parent.Right = null;
                }
                return;
            }
            //Если нет левого дочернего, то правый дочерний становится на место удаляемого
            if (node.Left == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = node.Right;
                }
                else
                {
                    node.Parent.Right = node.Right;
                }

                node.Right.Parent = node.Parent;
                return;
            }
            //Если нет правого дочернего, то левый дочерний становится на место удаляемого
            if (node.Right == null)
            {
                if (me == BinSide.Left)
                {
                    node.Parent.Left = node.Left;
                }
                else
                {
                    node.Parent.Right = node.Left;
                }

                node.Left.Parent = node.Parent;
                return;
            }

            //Если присутствуют оба дочерних узла
            //то правый ставим на место удаляемого
            //а левый вставляем в правый

            if (me == BinSide.Left)
            {
                node.Parent.Left = node.Right;
            }
            if (me == BinSide.Right)
            {
                node.Parent.Right = node.Right;
            }
            if (me == null)
            {
                var bufLeft = node.Left;
                var bufRightLeft = node.Right.Left;
                var bufRightRight = node.Right.Right;
                node.Data = node.Right.Data;
                node.Right = bufRightRight;
                node.Left = bufRightLeft;
                Insert(bufLeft, node, node);
            }
            else
            {
                node.Right.Parent = node.Parent;
                Insert(node.Left, node.Right, node.Right);
            }
        }
        public long CountElements()
        {
            return CountElements(this);
        }

        private long CountElements(BinaryTree<T> node)
        {
            long count = 1;
            if (node.Right != null)
            {
                count += CountElements(node.Right);
            }
            if (node.Left != null)
            {
                count += CountElements(node.Left);
            }
            return count;
        }

    }
}
