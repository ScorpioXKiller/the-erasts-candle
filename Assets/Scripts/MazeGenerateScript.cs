﻿using System.Collections.Generic;
using UnityEngine;
using Assets;
using System.Diagnostics;
using Grid = Assets.Scripts.PathFinding.Grid;
using Assets.Scripts.PathFinding;

public static class RoomParts
{



    public const string SideWallName = "SideWall";
    public const string BottomWallName = "BottomWall";
    public const string TopWallName = "TopWall";

    public const string SideWallDoorName = "SideWallDoor";
    public const string SideWallNoDoorName = "SideWallNoDoor";
    public const string HorizontalWallDoorName = "HorizontalWallDoor";
    public const string HorizontalWallNoDoorName = "HorizontalWallNoDoor";

    public const string FloorName = "Floor";

}


public class MazeGenerateScript : MonoBehaviour
{

    public AudioSource backGroundSound;
    public GameObject end;
    public int MazeSize = 10;

    public int BigRoomStart => MazeSize / 2 - 1;
    public int BigRoomSize = 2;

    public Grid grid;

    private string PathToRooms = @"Rooms/";

    private int RoomsGetCount = 1;

    public static GameObject[,] floors;


    private List<RoomForRender> rooms = new List<RoomForRender>();

    private Bounds WholeMazeBounds;
    private Cell[,] MazeMatrix;

    private int RndRoom
    {
        get
        {
            return Random.Range(0, RoomsGetCount);
        }
    }


    void Start()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        floors = new GameObject[MazeSize, MazeSize];
        Maze maze = new Maze(MazeSize);
        maze.FillMaze();
        for (int i = 0; i < BigRoomSize; i++)
        {
            maze.LockRoom(BigRoomStart, BigRoomStart + i);
            maze.LockRoom(BigRoomStart + 1, BigRoomStart + i);
        }
        MazeMatrix = maze.BuildMaze();
        LoadRoomsFromResources();
        UnityEngine.Debug.Log("Generate maze for " + stopwatch.ElapsedMilliseconds + "ms");

        stopwatch.Restart();

        FillMazeWithRooms();

        SetMazeBounds();

        PutEndPoint();

        PutGameObjectToRoomCenter(GameObject.FindGameObjectWithTag("Player"), 0, 0);
        PutGameObjectToRoomCenter(GameObject.Find("EnemyMask"), 9, 9);

        PutGameObjectToRoomCenter(GameObject.Find("ChestEye"), 0, 1);

        UnityEngine.Debug.Log("Render maze for " + stopwatch.ElapsedMilliseconds + "ms");
        stopwatch.Stop();

        if (grid != null)
        {
            Node[,] mazeGrid = new Node[MazeMatrix.GetLength(0), MazeMatrix.GetLength(1)];
            var step = (floors[0, 0].GetObjectBounds() / 2);
            step.y *= -1;
            for (int i = 0; i < MazeMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < MazeMatrix.GetLength(1); j++)
                {
                    var mazeCell = MazeMatrix[i, j];
                    if (i % 2 == 1 && j % 2 == 1)
                    {
                        mazeGrid[i, j] = new Node(floors[(i - 1) / 2, (j - 1) / 2].transform.position + step, mazeCell.IsWall, i, j);
                    }
                    else
                        mazeGrid[i, j] = new Node(new Vector3(), mazeCell.IsWall, i, j);
                }
            }
            grid.SetGridSizeAndCreate(WholeMazeBounds.size, mazeGrid);
            //grid.GetComponent<PathFinding>().target = end.transform;
        }

        //Navigation



    }

    void Update()
    {

    }

    private void SetMazeBounds()
    {
        Vector3 center = Vector3.zero;
        foreach (Transform child in transform)
        {
            center += child.GetComponent<Renderer>().bounds.center;
        }
        center /= transform.childCount;
        WholeMazeBounds = new Bounds(center, Vector3.zero);
        foreach (Transform child in transform)
        {
            WholeMazeBounds.Encapsulate(child.gameObject.GetComponent<Renderer>().bounds);
        }
    }

    private void PutGameObjectToRoomCenter(GameObject gameObject, int i, int j)
    {
        //var tempRoom = floors.First(f => GetJFromPositionX(f.transform.position.x) == j && GetIFromPositionY(f.transform.position.y) == i);
        var tempRoom = floors[i, j];
        gameObject.transform.position = new Vector3(
            tempRoom.transform.position.x + tempRoom.GetObjectBounds().x / 2,
            tempRoom.transform.position.y - tempRoom.GetObjectBounds().y / 2);
        //gameObject.transform.position.z);
    }

    private void PutEndPoint()
    {
        end = Instantiate(end, transform);
        //end.transform.position = new Vector3(WholeMazeBounds.size.x - rooms[0].SideWall.GetObjectBounds().x, (-WholeMazeBounds.size.y - rooms[0].SideWall.GetObjectBounds().y), end.transform.position.z);
        end.transform.position = new Vector3(floors[MazeSize - 1, MazeSize - 1].transform.position.x + floors[MazeSize - 1, MazeSize - 1].GetObjectBounds().x - 0.1f,
            floors[MazeSize - 1, MazeSize - 1].transform.position.y - floors[MazeSize - 1, MazeSize - 1].GetObjectBounds().y + 0.1f, end.transform.position.z);
    }

    private void LoadRoomsFromResources()
    {
        for (int i = 0; i < RoomsGetCount; i++)
        {
            rooms.Add(new RoomForRender(Resources.LoadAll<GameObject>(PathToRooms + (i + 1))));
        }
    }

    private void FillMazeWithRooms()
    {


        for (int i = 1; i < MazeMatrix.GetLength(0) - 1; i++)
        {
            for (int j = 1; j < MazeMatrix.GetLength(1) - 1; j++)
            {
                BuildRoomPart(rooms[RndRoom], i, j);
            }
        }


        for (int j = 0; j < MazeSize; j++)
        {
            BuildTop(rooms[RndRoom], j);
            BuildBottom(rooms[RndRoom], j);
            BuildLeftSide(rooms[RndRoom], j);
            BuildRightSide(rooms[RndRoom], j);
        }




    }

    //private int GetJFromPositionX(float x)
    //{
    //    return (int)System.Math.Round(((x - rooms[0].WallSize.y - (rooms[0].FloorSize.x / 2)) / rooms[0].Width));
    //}
    //private int GetIFromPositionY(float y)
    //{
    //    return (int)System.Math.Round(((y + rooms[0].FloorSize.y / 2 + rooms[0].BigWallSize.y) / rooms[0].Height) * -1);
    //}


    private void BuildFloor(RoomForRender room, int i, int j)
    {
        var floor = Instantiate(room.Floor, transform);
        floors[i, j] = floor;

        var FloorBound = floor.GetObjectBounds();
        floor.transform.position = new Vector3(room.SideWall.GetObjectBounds().x + (FloorBound.x + room.SideWallDoor.GetObjectBounds().x) * j,
             -(room.TopWall.GetObjectBounds().y + (FloorBound.y + room.HorizontalWallDoor.GetObjectBounds().y) * i));


    }

    private void BuildVerticalWall(RoomForRender room, int i, int j, bool IsWall)
    {
        var wall = Instantiate(IsWall ? room.SideWallNoDoor : room.SideWallDoor, transform);
        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3(room.SideWall.GetObjectBounds().x - WallBound.x + (room.Floor.GetObjectBounds().x + WallBound.x) * j,
           -(room.TopWall.GetObjectBounds().y + (room.HorizontalWallNoDoor.GetObjectBounds().y + WallBound.y) * i));
    }

    private void BuildHorizontalWall(RoomForRender room, int i, int j, bool IsWall)
    {
        var wall = Instantiate(IsWall ? room.HorizontalWallNoDoor : room.HorizontalWallDoor, transform);
        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3((WallBound.x + room.SideWallDoor.GetObjectBounds().x - room.SideWall.GetObjectBounds().x * 2) * j,
           -(room.TopWall.GetObjectBounds().y - WallBound.y + (room.Floor.GetObjectBounds().y + WallBound.y) * i));
    }

    private void BuildTop(RoomForRender room, int index)
    {
        var wall = Instantiate(room.TopWall, transform);
        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3((WallBound.x + room.SideWallDoor.GetObjectBounds().x - room.SideWall.GetObjectBounds().x * 2) * index,
            0);
    }
    private void BuildBottom(RoomForRender room, int index)
    {
        var wall = Instantiate(room.BottomWall, transform);
        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3((WallBound.x + room.SideWallDoor.GetObjectBounds().x - room.SideWall.GetObjectBounds().x * 2) * index,
            floors[MazeSize - 1, MazeSize - 1].transform.position.y - floors[MazeSize - 1, MazeSize - 1].GetObjectBounds().y);
    }
    private void BuildLeftSide(RoomForRender room, int index)
    {
        var wall = Instantiate(room.SideWall, transform);

        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3(0,
            -(room.TopWall.GetObjectBounds().y + (WallBound.y + room.HorizontalWallDoor.GetObjectBounds().y) * index));

    }
    private void BuildRightSide(RoomForRender room, int index)
    {
        var wall = Instantiate(room.SideWall, transform);

        var WallBound = wall.GetObjectBounds();
        wall.transform.position = new Vector3(floors[MazeSize - 1, MazeSize - 1].transform.position.x + floors[MazeSize - 1, MazeSize - 1].GetObjectBounds().x + WallBound.x,
            -(room.TopWall.GetObjectBounds().y + (WallBound.y + room.HorizontalWallDoor.GetObjectBounds().y) * index));
        var temp = wall.transform.localScale;
        temp.x *= -1;
        wall.transform.localScale = temp;

    }
    private void BuildRoomPart(RoomForRender room, int i, int j)
    {
        if (i % 2 == 1 && j % 2 == 1)
        {
            BuildFloor(room, (i - 1) / 2, (j - 1) / 2);
        }
        else if (i % 2 == 1 && j % 2 == 0)
        {
            BuildVerticalWall(room, (i - 1) / 2, j / 2, MazeMatrix[i, j].IsWall);
        }
        else if (i % 2 == 0 && j % 2 == 1)
        {
            BuildHorizontalWall(room, i / 2, (j - 1) / 2, MazeMatrix[i, j].IsWall);
        }
    }



    //private void BuildHorizontalWall(RoomForRender room, int i, int j, bool IsWall)
    //{
    //    var wall = Instantiate(IsWall ? room.WallNoDoor : room.WallWithDoor, transform);
    //    wall.transform.position = new Vector3(room.WallSize.y + room.WallSize.x / 2 + room.Width * j,
    //        -room.Height * i + room.WallSize.y / 2);
    //}
    //private void BuildVerticalWall(RoomForRender room, int i, int j, bool IsWall)
    //{
    //    var wall = Instantiate(IsWall ? room.WallNoDoor : room.WallWithDoor, transform);
    //    wall.transform.position = new Vector3(room.WallSize.y / 2 + room.Width * j,
    //      -room.WallSize.x / 2 - room.Height * i);
    //    wall.transform.rotation = Quaternion.Euler(new Vector3(0, 0, transform.rotation.eulerAngles.z + 90));
    //}
    //private void BuildFloor(RoomForRender room, int i, int j)
    //{
    //    var floor = Instantiate(room.Floor, transform);
    //    floors.Add(floor);
    //    floor.transform.position = new Vector3(room.WallSize.y + room.FloorSize.x / 2 + room.Width * j,
    //        -room.FloorSize.y / 2 - room.BigWallSize.y - room.Height * i);
    //}
}


public class RoomForRender
{
    //public GameObject WallWithDoor;
    //public GameObject WallNoDoor;
    //public GameObject BigWallWithDoor;
    //public GameObject BigWallNoDoor;
    //public GameObject Floor;
    public GameObject SideWall;
    public GameObject BottomWall;
    public GameObject TopWall;

    public GameObject SideWallDoor;
    public GameObject SideWallNoDoor;
    public GameObject HorizontalWallDoor;
    public GameObject HorizontalWallNoDoor;

    public GameObject Floor;







    public RoomForRender(object[] objects)
    {
        foreach (GameObject item in objects)
        {


            switch (item.name)
            {
                case RoomParts.BottomWallName:
                    BottomWall = item;
                    break;
                case RoomParts.FloorName:
                    Floor = item;
                    break;
                case RoomParts.HorizontalWallDoorName:
                    HorizontalWallDoor = item;
                    break;
                case RoomParts.HorizontalWallNoDoorName:
                    HorizontalWallNoDoor = item;
                    break;
                case RoomParts.SideWallDoorName:
                    SideWallDoor = item;
                    break;
                case RoomParts.SideWallName:
                    SideWall = item;
                    break;
                case RoomParts.SideWallNoDoorName:
                    SideWallNoDoor = item;
                    break;
                case RoomParts.TopWallName:
                    TopWall = item;
                    break;
                default:
                    break;
            }


        }
    }
}

public static class Helper
{
    public static Vector3 GetObjectBounds(this GameObject @object)
    {
        return @object.GetComponent<Renderer>().bounds.size;
    }
}


