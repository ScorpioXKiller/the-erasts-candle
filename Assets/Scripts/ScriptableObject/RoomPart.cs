﻿namespace Assets.Scripts.ScriptableObject
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "NewRoomPart",menuName = "RoomPart")]
    public class RoomPart : ScriptableObject
    {
        public GameObject RoomPartObject;
    }
}
